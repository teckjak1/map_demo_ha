package com.thuha.mapdemo;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

public class MainActivity extends AppCompatActivity implements ActionCallBack {

    private static final String[] PEMISSTION_MAP = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_NETWORK_STATE};
    private static final int REQUEST_CODE = 101;
    private static final float ZOOM16 = 16;
    private GoogleMap mMap;
    private LocationMgr locationMgr;
    private Marker myMarker;
    private Location myLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        checkPermissions();

    }

    private void checkPermissions() {
        for (String permission : PEMISSTION_MAP) {
            if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(PEMISSTION_MAP, REQUEST_CODE);
                return;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode != REQUEST_CODE) {
            finish();
        }
        for (int rs : grantResults) {
            if (rs != PackageManager.PERMISSION_GRANTED) {
                return;
            }
        }
    }

    private void initViews() {
        SupportMapFragment mapFrg = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_frag);
        mapFrg.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                initData();
            }
        });

        locationMgr = new LocationMgr(this, this);
    }

    private void initData() {
        //set cu chi tay
        mMap.getUiSettings().setAllGesturesEnabled(true);
        //set chi duong
        mMap.getUiSettings().setMapToolbarEnabled(true);
        //hien thi nut de ve vi tri hien tai
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        //hien thi vi tri hien tai
        mMap.setMyLocationEnabled(true);

        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        mMap.setTrafficEnabled(true);
        mMap.setIndoorEnabled(true);
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                MainActivity.this.showDirections(marker.getPosition());
            }
        });

        addMarker("TechJA", "So 5 Trung Kinh", new LatLng(21.017002, 105.799344));
        initInfoWindow();

    }

    public void initInfoWindow() {
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return initInfoView(marker);
            }

            @Override
            public View getInfoContents(Marker marker) {
                return null;
            }
        });
    }

    private View initInfoView(final Marker marker) {
        View v = LayoutInflater.from(this).inflate(R.layout.item_view, null);
        ImageView ivPhoto = v.findViewById(R.id.image);
        TextView tvTitile = v.findViewById(R.id.tv_title);
        TextView tvContent = v.findViewById(R.id.tv_content);
        v.findViewById(R.id.direction);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDirections(marker.getPosition());
                Toast.makeText(getApplicationContext(), "hahah", Toast.LENGTH_SHORT).show();
            }
        });

        ivPhoto.setImageResource(R.drawable.image);
        tvTitile.setText(marker.getTitle());
        tvContent.setText(marker.getSnippet());
        return v;
    }

    private void showDirections(LatLng position) {
        if (myLocation == null) {
            return;
        }
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse(String.format("http://maps.google.com/maps?saddr=%s,%s&daddr=%s,%s", myLocation.getLatitude(), myLocation.getLongitude(), position.latitude, position.longitude)));
        startActivity(intent);
    }

    public Marker addMarker(String title, String content, LatLng myLocation) {
        MarkerOptions marker = new MarkerOptions();
        marker.title(title);
        marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));
        marker.snippet(content);
//        LatLng location = new LatLng(21.013643, 105.801835);
        marker.position(myLocation);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, ZOOM16));
        Marker mMarker = mMap.addMarker(marker);
        mMarker.showInfoWindow();
        return mMarker;
    }

    @Override
    public void callBack(Object obj, String key) {
        if (key == LocationMgr.KEY_LOCATION) {
            List<Location> locationList = (List<Location>) obj;
            if (locationList == null || locationList.size() == 0) {
                return;
            }
            myLocation = locationList.get(0);
            if (myMarker == null) {
                myMarker = addMarker("MyLocation", "Unknown", new LatLng(myLocation.getLatitude(), myLocation.getLongitude()));
            } else {
                myMarker.setPosition(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()));
            }
        }
    }
}
