package com.thuha.mapdemo;

import android.content.Context;
import android.os.Looper;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;

public class LocationMgr {
    private static final long REQUEST_TIME_UPDATE = 2000;
    private static final String TAG = "TAG";
    public static final String KEY_LOCATION = "KEY_LOCATION";
    private Context context;
    private FusedLocationProviderClient client;
    private ActionCallBack mCallBack;

    public LocationMgr(Context context, ActionCallBack callBack) {
        this.context = context;
        mCallBack = callBack;
        initLocation();
    }

    private void initLocation() {
        client = new FusedLocationProviderClient(context);
        LocationRequest request = new LocationRequest();
        request.setInterval(REQUEST_TIME_UPDATE);
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationCallback callback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                updateLocation(locationResult);
            }
        };
        client.requestLocationUpdates(request,callback, Looper.getMainLooper());
    }

    private void updateLocation(LocationResult locationResult) {
        Log.i(TAG,"updateLocation"+locationResult.toString());
        mCallBack.callBack(locationResult.getLocations(), KEY_LOCATION);
    }
}
